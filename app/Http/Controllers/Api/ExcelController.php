<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\MExcelrow;

use Session;

//use App\Exports\SiswaExport;
use App\Imports\ExcelImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class ExcelController extends Controller
{

  
	public function index()
	{
		$siswa = MExcelrow::all();
		return view('siswa',['excel_row'=>$siswa]);
	}

	public function export_excel()
	{
		return Excel::download(new SiswaExport, 'siswa.xlsx');
	}

	public function import_excel(Request $request) 
	{
		// validasi
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);

		// menangkap file excel
		$file = $request->file('file');

		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();

		// upload ke folder file_siswa di dalam folder public
		$file->move('file_siswa',$nama_file);

		// import data
		Excel::import(new ExcelImport, public_path('/file_siswa/'.$nama_file));

		// notifikasi dengan session
		Session::flash('sukses','Data Berhasil Diimport!');

		// alihkan halaman kembali
        //return redirect('/siswa');
        //return redirect('/');
	}
}