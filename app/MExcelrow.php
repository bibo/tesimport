<?php
 
namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class MExcelrow extends Model
{
    protected $table = "excel_row";
    //PDF Title	Row Sequence Number	Page Number	Total Pages	Document Title	Language	Typed, Handwritten, or Both?	Wording	Option Selected	Option Not Selected	Misspelled Words Fixed
    protected $fillable = ['PDF Title',	'Row Sequence Number',	'Page Number',	'Total Pages',	'Document Title', 'Language',	'Typed, Handwritten, or Both?',	'Wording',	'Option Selected',	'Option Not Selected',	'Misspelled Words Fixed'];
}