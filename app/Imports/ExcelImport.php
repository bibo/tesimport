<?php

namespace App\Imports;

use App\MExcelrow;
use Maatwebsite\Excel\Concerns\ToModel;

class ExcelImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        
        return new MExcelrow([
            'PDF Title' => $row[0],
            'Row Sequence Number' => $row[1],
            'Page Number' => $row[2],
            'Total Pages' => $row[3],
            'Document Title' => $row[4],
            'Language' => $row[5],
            'Typed, Handwritten, or Both?' => $row[6],
            'Wording' => $row[7],
            'Option Selected' => $row[8],
            'Option Not Selected' => $row[9],
            'Misspelled Words Fixed' => $row[10]
        ]);
    }
}